var _ = require('lodash'),
    request = require('request'),
    util = require('./util'),
    querystring = require('querystring'),
    pickInputs = {
        list_id: {
            key: 'list_id',
            validate: {
                req: true,
                check: 'checkAlphanumeric'
            }
        },
        subscriber_hash: {
            key: 'subscriber_hash',
            validate: {
                req: true,
                check: 'checkAlphanumeric'
            }
        }
    },
    pickOutputs = {
        success: 'success'
    };

module.exports = {
    authOptions: function (dexter) {
        if (!dexter.environment('mailchimp_api_key') || !dexter.environment('server')) {
            this.fail('A [mailchimp_api_key] and [server] environment variables are required for this module');
            return false;
        } else {
            return {
                api_key: dexter.environment('mailchimp_api_key'),
                server: dexter.environment('server')
            }
        }
    },
    /**
     * The main entry point for the Dexter module
     *
     * @param {AppStep} step Accessor for the configuration for the step using this module.  Use step.input('{key}') to retrieve input data.
     * @param {AppData} dexter Container for all data used in this workflow.
     */
    run: function(step, dexter) {
        var auth = this.authOptions(dexter);

        if (!auth) return;

        var inputs = util.pickInputs(step, pickInputs),
          validateErrors = util.checkValidateErrors(inputs, pickInputs);

        if (validateErrors)
            return this.fail(validateErrors);

        var username = "api_key",
            uri = 'lists/' + inputs.list_id + '/members/' + inputs.subscriber_hash,
            reqAuth = "Basic " + new Buffer(username + ":" + auth.api_key).toString("base64"),
            baseUrl = 'https://' + auth.server + '.api.mailchimp.com/3.0/';

        request({
            method: 'DELETE',
            baseUrl: baseUrl,
            uri: uri,
            json: true,
            headers : {
                "Authorization" : reqAuth
            }
        },
        function (error, response, body) {
            if (!error && response.statusCode == 204) {
                this.complete({status: 'Success'});
            } else {
                this.fail(error || body);
            }
        }.bind(this));
    }
};
