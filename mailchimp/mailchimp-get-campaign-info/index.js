var _ = require('lodash'),
    request = require('request'),
    util = require('./util'),
    querystring = require('querystring'),
    pickInputs = {
        campaign_id: {
            key: 'campaign_id',
            validate: {
                req: true
            }
        },
        fields: {
            key: 'fields',
            type: 'array'
        },
        exclude_fields: {
            key: 'exclude_fields',
            type: 'array'
        }
    },
    pickOutputs = {
        id: 'id',
        type: 'type',
        create_time: 'create_time',
        archive_url: 'archive_url',
        status: 'status',
        emails_sent: 'emails_sent',
        report_summary: 'report_summary',
        _links: '_links'
    };

module.exports = {
    authOptions: function (dexter) {
        if (!dexter.environment('mailchimp_api_key') || !dexter.environment('server')) {
            this.fail('A [mailchimp_api_key] and [server] environment variables are required for this module');
            return false;
        } else {
            return {
                api_key: dexter.environment('mailchimp_api_key'),
                server: dexter.environment('server')
            }
        }
    },
    /**
     * The main entry point for the Dexter module
     *
     * @param {AppStep} step Accessor for the configuration for the step using this module.  Use step.input('{key}') to retrieve input data.
     * @param {AppData} dexter Container for all data used in this workflow.
     */
    run: function(step, dexter) {
        var auth = this.authOptions(dexter);

        if (!auth)
            return;

        var inputs = util.pickInputs(step, pickInputs),
          validateErrors = util.checkValidateErrors(inputs, pickInputs);

        if (validateErrors)
            return this.fail(validateErrors);

        if (inputs.campaign_id) {
            var newInputs = _.omit(inputs, 'campaign_id');
        }

        if (newInputs.fields)
            newInputs.fields = _.map(newInputs.fields, function(value) {return value.trim()}).join();

        if (newInputs.exclude_fields)
            newInputs.exclude_fields = _.map(newInputs.exclude_fields, function(value) {return value.trim()}).join();

        var queryParams = querystring.stringify(newInputs),
            username = "api_key",
            uri = queryParams ? 'campaigns/' + inputs.campaign_id + '?' + queryParams : 'campaigns/' + inputs.campaign_id,
            reqAuth = "Basic " + new Buffer(username + ":" + auth.api_key).toString("base64"),
            baseUrl = 'https://' + auth.server + '.api.mailchimp.com/3.0/';

        request({
            baseUrl: baseUrl,
            method: 'GET',
            uri: uri,
            json: true,
            headers : {
                "Authorization" : reqAuth
            }
        },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                this.complete(util.pickOutputs(body, pickOutputs));
            } else {
                this.fail(error || body);
            }
        }.bind(this));
    }
};
