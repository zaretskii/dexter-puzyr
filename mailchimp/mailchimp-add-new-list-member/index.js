var _ = require('lodash'),
    request = require('request'),
    util = require('./util'),
    querystring = require('querystring'),
    pickInputs = {
        list_id: {
            key: 'list_id',
            validate: {
                req: true,
                check: 'checkAlphanumeric'
            }
        },
        email_address: {
            key: 'email_address',
            validate: {
                req: true,
                check: 'isEmail'
            }
        },
        email_type: 'email_type',
        status: {
            key: 'status',
            validate: {
                req: true
            }
        },
        merge_fields: {
            key: 'merge_fields'
        },
        language: 'language',
        vip: 'vip'
    },
    pickOutputs = {
        id: 'id',
        email_address: 'email_address',
        unique_email_id: 'unique_email_id',
        email_type: 'email_type',
        status: 'status',
        merge_fields: 'merge_fields',
        language: 'language',
        _links: '_links'
    };

module.exports = {
    authOptions: function (dexter) {
        if (!dexter.environment('mailchimp_api_key') || !dexter.environment('server')) {
            this.fail('A [mailchimp_api_key] and [server] environment variables are required for this module');
            return false;
        } else {
            return {
                api_key: dexter.environment('mailchimp_api_key'),
                server: dexter.environment('server')
            }
        }
    },
    /**
     * The main entry point for the Dexter module
     *
     * @param {AppStep} step Accessor for the configuration for the step using this module.  Use step.input('{key}') to retrieve input data.
     * @param {AppData} dexter Container for all data used in this workflow.
     */
    run: function(step, dexter) {
        var auth = this.authOptions(dexter);

        if (!auth) return;

        var inputs = util.pickInputs(step, pickInputs),
          validateErrors = util.checkValidateErrors(inputs, pickInputs);

        if (validateErrors)
            return this.fail(validateErrors);

        if (inputs.status) {
            var checkStatusArray = inputs.status.split(/[\s,]+/);
            if(_.difference(checkStatusArray, ['subscribed', 'unsubscribed', 'cleaned', 'pending']).length != 0) {
                return this.fail('[status]. Possible values: "subscribed", "unsubscribed", "cleaned", "pending"');
            }
        }

        var newInputs = _.omit(inputs, 'list_id');
            username = "api_key",
            uri = 'lists/' + inputs.list_id + '/members',
            reqAuth = "Basic " + new Buffer(username + ":" + auth.api_key).toString("base64"),
            baseUrl = 'https://' + auth.server + '.api.mailchimp.com/3.0/';

        request({
            method: 'POST',
            baseUrl: baseUrl,
            uri: uri,
            json: true,
            body: newInputs,
            headers : {
                "Authorization" : reqAuth
            }
        },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                this.complete(util.pickOutputs(body, pickOutputs));
            } else {
                this.fail(error || body);
            }
        }.bind(this));
    }
};
