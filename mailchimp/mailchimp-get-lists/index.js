var _ = require('lodash'),
    request = require('request'),
    util = require('./util'),
    querystring = require('querystring'),
    pickInputs = {
        fields: {
            key: 'fields',
            type: 'array'
        },
        exclude_fields: {
            key: 'exclude_fields',
            type: 'array'
        },
        count: {
            key: 'count',
            validate: {
                check: 'isInteger'
            }
        },
        before_send_time: 'before_send_time',
        before_create_time: 'before_create_time'

    },
    pickOutputs = {
        total_items: 'total_items',
        id: { keyName: 'lists', fields: ['id']},
        name: { keyName: 'lists', fields: ['name']},
        contact: { keyName: 'lists', fields: ['contact']},
        campaign_defaults: { keyName: 'lists', fields: ['campaign_defaults']},
        subscribe_url_short: { keyName: 'lists', fields: ['subscribe_url_short']},
        stats: { keyName: 'lists', fields: ['stats']},
        _links: { keyName: 'lists', fields: ['_links']}
    };

module.exports = {
    authOptions: function (dexter) {
        if (!dexter.environment('mailchimp_api_key') || !dexter.environment('server')) {
            this.fail('A [mailchimp_api_key] and [server] environment variables are required for this module');
            return false;
        } else {
            return {
                api_key: dexter.environment('mailchimp_api_key'),
                server: dexter.environment('server')
            }
        }
    },
    /**
     * The main entry point for the Dexter module
     *
     * @param {AppStep} step Accessor for the configuration for the step using this module.  Use step.input('{key}') to retrieve input data.
     * @param {AppData} dexter Container for all data used in this workflow.
     */
    run: function(step, dexter) {
        var auth = this.authOptions(dexter);

        if (!auth) return;

        var inputs = util.pickInputs(step, pickInputs),
          validateErrors = util.checkValidateErrors(inputs, pickInputs);

        if (validateErrors)
            return this.fail(validateErrors);

        if (inputs.fields)
            inputs.fields = _.map(inputs.fields, function(value) {return value.trim()}).join();

        if (inputs.exclude_fields)
            inputs.exclude_fields = _.map(inputs.exclude_fields, function(value) {return value.trim()}).join();

        var queryParams = querystring.stringify(inputs),
            username = "api_key",
            uri = queryParams ? 'lists?' + queryParams : 'lists',
            reqAuth = "Basic " + new Buffer(username + ":" + auth.api_key).toString("base64"),
            baseUrl = 'https://' + auth.server + '.api.mailchimp.com/3.0/';

        request({
            baseUrl: baseUrl,
            method: 'GET',
            uri: uri,
            json: true,
            headers : {
                "Authorization" : reqAuth
            }
        },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                this.complete(util.pickOutputs(body, pickOutputs));
            } else {
                this.fail(error || body);
            }
        }.bind(this));
    }
};
