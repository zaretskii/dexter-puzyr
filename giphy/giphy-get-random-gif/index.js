var _ = require('lodash'),
    util = require('./util'),
    giphy = require('giphy-api'),
    pickInputs = {
        tag: {
            key: 'tag',
            validate: {
                check: 'checkAlphanumeric'
            }
        }
    },
    pickOutputs = {
        url: 'data.fixed_height_downsampled_url'
    };

module.exports = {
    authOptions: function (dexter) {
        if (!dexter.environment('giphy_api_key')) {
            this.fail('A [giphy_api_key] variable is required for this module');
            return false;
        } else {
            return {
                api_key: dexter.environment('giphy_api_key')
            }
        }
    },
    /**
     * The main entry point for the Dexter module
     *
     * @param {AppStep} step Accessor for the configuration for the step using this module.  Use step.input('{key}') to retrieve input data.
     * @param {AppData} dexter Container for all data used in this workflow.
     */
    run: function(step, dexter) {
        var auth = this.authOptions(dexter);

        if (!auth) return;

        giphy = giphy(auth.api_key);

        var inputs = util.pickInputs(step, pickInputs),
          validateErrors = util.checkValidateErrors(inputs, pickInputs);

        if (validateErrors)
            return this.fail(validateErrors);
        giphy.random({
            tag: inputs.tag,
            fmt: 'gif'
        }, function(err, res) {

            if (err)
                return this.fail(validateErrors);

            this.complete(util.pickOutputs(res, pickOutputs));
        }.bind(this));
    }
};
